import { Component } from '@angular/core';
import { PaisService } from '../../services/pais.service';
import { respuesta } from '../../interfaces/logicaPais';
import { thePais } from '../../interfaces/pais.interface';


@Component({
  selector: 'app-por-pais',
  templateUrl: './por-pais.component.html',
  styles: []
})
export class PorPaisComponent {

  termino: string = '';
  hayError: boolean = false;
  paises : thePais[] = [];

  constructor(private paisService: PaisService) { }
  
  sugerencias(event: any){
    this.hayError = false;
    //ToDO: crear  sugerencias
  }

  buscar(termino: string){
    this.hayError = false;
    this.termino  = termino;

    if(this.termino.trim().length > 0){
      this.paisService.buscarPais(this.termino)
        .subscribe({ 
          next: (res) => {
            let pais = respuesta(res, this.termino);
            this.paises = pais;
          }, 
          error: (err) => {
            this.hayError = true;
          },
          complete: () => {
            this.hayError = false;
          }
        });
    }
  }
}
