import { Component } from '@angular/core';
import { thePais } from '../../interfaces/pais.interface';
import { PaisService } from '../../services/pais.service';
import { respuesta } from '../../interfaces/logicaPais';

@Component({
  selector: 'app-por-capital',
  templateUrl: './por-capital.component.html',
  styles: [
  ]
})
export class PorCapitalComponent {
  termino: string = '';
  hayError: boolean = false;
  paises : thePais[] = [];

  constructor(private paisService: PaisService) { }

  sugerencias(event: any){
    this.hayError = false;
    //ToDO: crear  sugerencias
  }

  buscar(termino: string){
    this.hayError = false;
    this.termino  = termino;

    if(this.termino.trim().length > 0){
      this.paisService.buscarCapital(this.termino)
        .subscribe({ 
          next: (res) => {
            let pais = respuesta(res, this.termino);
            this.paises = pais;
          }, 
          error: (err) => {
            this.hayError = true;
          },
          complete: () => {
            this.hayError = false;
          }
        });
    }
  }

}
