import { Component } from '@angular/core';
import { Country, thePais } from '../../interfaces/pais.interface';
import { PaisService } from '../../services/pais.service';
import { respuesta } from '../../interfaces/logicaPais';

@Component({
  selector: 'app-por-region',
  templateUrl: './por-region.component.html',
  styles: [`
    button{
      margin-right:5px;
    }
    `
  ]
})
export class PorRegionComponent{
  regiones: string[] = ['africa','americas', 'asia', 'europe','oceania']
  regionActiva: string ='';
  paises: thePais[] = [];
  constructor(private ServiceApi: PaisService) { }
  
  getClasseCSS(region: string){
    return (region === this.regionActiva) ? 'btn btn-primary' : 'btn btn-outline-primary' 
  }

  ActivarRegion(region: string){
    
    if( region === this.regionActiva ) { return ;}

    this.regionActiva = region
    this.paises = [];

    this.ServiceApi.buscarRegion(region)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.paises = respuesta(res,'');          
          
        },
        error: (error) => '',
        complete: () => ''
      });
  }
}
