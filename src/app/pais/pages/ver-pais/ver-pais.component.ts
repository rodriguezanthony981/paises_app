import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PaisService } from '../../services/pais.service';
import { thePais } from '../../interfaces/pais.interface';
import { respuesta, returnLengua } from '../../interfaces/logicaPais';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-ver-pais',
  templateUrl: './ver-pais.component.html',
  styles: [
  ]
})
export class VerPaisComponent implements OnInit{
  
  public paiss: thePais[] = [];
  id: string = '';
  lengua!: any;
  
  constructor(
    private activatedRoute: ActivatedRoute,
    private paisService:PaisService
    ) { }

  ngOnInit(): void {
    this.activatedRoute.params
      .pipe(
        switchMap( ({ id }) => {
          this.id = id
          return this.paisService.getPaisPorAlpha(id)
        } ),
      )
      .subscribe({
        next: (res) => {
          this.paiss = respuesta(res, this.id);
          this.lengua = returnLengua(this.paiss[0].languages);
        },
        error: (error) => {
          console.error(error);
        },
        complete: () => {
          console.log('Data -  -');
        }
      });
  }

} 
