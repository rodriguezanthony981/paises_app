import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Country } from '../interfaces/pais.interface';

@Injectable({
  providedIn: 'root'
})
export class PaisService {

  private apiKey: string ='aTnJk3oUWc4rUZiMg9Q6eD6jbe7YXbXmtptT0CCB';
  private withApiKey: string = `?apikey=${this.apiKey}`
  private apiUrl: string = 'https://countryapi.io/api';
  
  
  constructor(private http: HttpClient) { }
  

  buscarPais(termino: string): Observable<Country> {
    const url = `${this.apiUrl}/name/${termino}${this.withApiKey}`
    
    return this.http.get<Country>(url);
  }

  buscarCapital(termino: string): Observable<Country> {
    const url = `${this.apiUrl}/capital/${termino}${this.withApiKey}`

    return this.http.get<Country>(url);
  }

  getPaisPorAlpha(id: string): Observable<Country>{
    const url = `${this.apiUrl}/name/${id}${this.withApiKey}`
    return this.http.get<Country>(url);
  }

  buscarRegion(termino: string): Observable<Country> {
    const url = `${this.apiUrl}/region/${termino}${this.withApiKey}`
    return this.http.get<Country>(url)
  }
}
