export interface Country {
  af: Af;
  sd: Sd;
  bi: Bi;
  mx: Mx;
  cu: Cu;
  rs: Rs;
  mc: Mc;
  bt: Bt;
  gy: Gy;
  gs: Gs;
  ba: Ba;
  bn: Bn;
  pk: Pk;
  ke: Ke;
  pr: Pr;
  so: So;
  sj: Sj;
  sl: Sl;
  pf: Nc;
  az: Az;
  ck: Ck;
  pe: Pe;
  bv: Bv;
  mp: Mp;
  ao: Ao;
  cg: Cg;
  ss: Ss;
  mf: Mc;
  tr: Tr;
  ai: AI;
  kn: Ag;
  aw: Aw;
  tc: Tc;
  tw: Tw;
  se: Se;
  lr: Lr;
  ve: Ve;
  vi: Tc;
  bm: Bm;
  al: Al;
  hk: Hk;
  lu: Lu;
  er: Er;
  co: Co;
  bq: Bq;
  mn: Mn;
  ye: Ye;
  lb: Lb;
  ag: Ag;
  vn: Vn;
  pw: Pw;
  je: Je;
  tt: Tt;
  il: Il;
  bg: Bg;
  pt: Pt;
  gi: Gi;
  sm: Sm;
  sg: Sg;
  sx: Sx;
  sa: Sa;
  gh: Gh;
  md: Md;
  td: Td;
  aq: Aq;
  gb: Gb;
  pg: PG;
  tf: Re;
  um: Um;
  bz: Bz;
  km: Km;
  bf: Bf;
  fo: Fo;
  gn: Gn;
  in: In;
  cw: Cw;
  tg: Bj;
  tn: Tn;
  bl: Bl;
  id: Id;
  fm: Fm;
  at: At;
  tj: Tj;
  cd: Cd;
  yt: Re;
  re: Re;
  ro: Ro;
  qa: Qa;
  lt: Lt;
  cn: Cn;
  nz: Nz;
  nf: Nf;
  mr: Mr;
  uz: Uz;
  fi: Fi;
  cm: Cm;
  hm: Hm;
  dm: AI;
  is: Is;
  om: Om;
  mk: Mk;
  li: Li;
  es: Es;
  gr: Gr;
  py: Py;
  bh: Bh;
  nu: Nu;
  sn: Sn;
  ms: Ag;
  lv: Lv;
  tk: Tk;
  jp: Jp;
  cf: Cf;
  ga: Ga;
  iq: Iq;
  im: Im;
  mm: Mm;
  me: Me;
  nr: Nr;
  vu: Vu;
  fr: Fr;
  zw: Zw;
  ph: Ph;
  sk: Sk;
  au: Au;
  ci: Bj;
  io: Io;
  sz: Sz;
  rw: Rw;
  bj: Bj;
  vg: Tc;
  ug: Ug;
  ie: Ie;
  ir: Ir;
  si: Si;
  ml: Ml;
  ch: Ch;
  as: As;
  uy: Uy;
  gu: Gu;
  sr: Sr;
  ua: Ua;
  cz: Cz;
  hn: Hn;
  ws: Ws;
  la: La;
  cv: Cv;
  et: Et;
  bd: Bd;
  sh: Sh;
  by: By;
  hr: Hr;
  kw: Kw;
  gf: Gf;
  ma: Ma;
  ru: Ru;
  ee: Ee;
  lk: Lk;
  nc: Nc;
  pl: Pl;
  mg: Mg;
  cr: Cr;
  sv: Ec;
  mo: Mo;
  ad: Ad;
  it: It;
  na: Na;
  sc: Sc;
  pa: Pa;
  ht: HT;
  ar: Ar;
  ne: Bf;
  mw: Mw;
  pn: Pn;
  de: De;
  ki: Ki;
  sy: Sy;
  mh: Mh;
  hu: Hu;
  ky: Ky;
  dk: Dk;
  lc: Lc;
  bo: Bo;
  dj: Dj;
  za: Za;
  ng: Ng;
  st: St;
  ni: Ni;
  gp: Bl;
  pm: Bl;
  ec: Ec;
  gl: Gl;
  gd: Ag;
  bs: Bs;
  cl: Cl;
  my: My;
  tv: Tv;
  cx: Cc;
  sb: Sb;
  tz: Tz;
  kp: Kp;
  gw: Gw;
  xk: Xk;
  va: Va;
  no: No;
  ps: Ps;
  cc: Cc;
  jm: Jm;
  eg: Eg;
  kh: Kh;
  mu: Mu;
  gm: Gm;
  gq: Gq;
  ls: Ls;
  mq: Bl;
  us: Us;
  eh: Eh;
  ae: Ae;
  mz: Mz;
  dz: Dz;
  zm: Zm;
  gt: Gt;
  kr: Kr;
  kg: Kg;
  tl: Tl;
  ax: Ax;
  jo: Jo;
  mt: Mt;
  cy: Cy;
  fk: Fk;
  kz: Kz;
  bw: Bw;
  vc: Ag;
  bb: Bb;
  to: To;
  th: Th;
  be: Be;
  ca: Ca;
  ge: Ge;
  wf: Wf;
  fj: Fj;
  nl: Nl;
  am: Am;
  do: Do;
  gg: Gg;
  tm: Tm;
  np: Np;
  mv: Mv;
  ly: Ly;
  br: Br;
}

export interface thePais {
  name:           string;
  official_name:  string;
  topLevelDomain: string[]|string;
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng | AqLatLng;
  demonyms:       AdDemonyms | CcDemonyms | string;
  area:           number;
  gini:           JmGini|BIGini|BdGini|string | AmGini | AEGini | AlGini | CFGini | BIGini | GyGini | GwGini | BdGini | AuGini | BjGini | BaGini | AzGini | KiGini | TtGini | MlGini | BzGini | CDGini | SyGini;
  timezones:      string[];
  borders:        string[] | string;
  nativeNames:    BWNativeNames|KzNativeNames|AENativeNames|AFNativeNames|AgNativeNames|MuNativeNames|KhNativeNames|JmNativeNames|NoNativeNames|KpNativeNames|KeNativeNames|MyNativeNames|ClNativeNames|GlNativeNames|DjNativeNames|BoNativeNames|DkNativeNames|HuNativeNames|MwNativeNames|ArNativeNames|HTNativeNames|ScNativeNames|MoNativeNames|ClNativeNames|MgNativeNames|PlNativeNames|LkNativeNames|EeNativeNames|RuNativeNames|MaNativeNames|HrNativeNames|ByNativeNames|BdNativeNames|EtNativeNames|LaNativeNames|UaNativeNames|IRNativeNames|SiNativeNames|RwNativeNames|KeNativeNames|SzNativeNames|AdNativeNames | MmNativeNames | AFNativeNames |IqNativeNames|CFNativeNames| ThNativeNames | BeNativeNames | ToNativeNames | CANativeNames | GeNativeNames | BfNativeNames | FjNativeNames | NlNativeNames | AmNativeNames | ClNativeNames | GgNativeNames | TmNativeNames | NPNativeNames | MvNativeNames | AENativeNames | AoNativeNames | SDNativeNames | AgNativeNames | RsNativeNames | BTNativeNames | { [key: string]: Cat } | BnNativeNames | PkNativeNames | KeNativeNames | SoNativeNames | BvNativeNames | AzNativeNames | BoNativeNames | AoNativeNames | CGNativeNames | TrNativeNames | AgNativeNames | CNNativeNames | AxNativeNames | AlNativeNames | HkNativeNames | ErNativeNames | MnNativeNames | DjNativeNames | VnNativeNames | JeNativeNames | ILNativeNames | BgNativeNames | ItNativeNames | SgNativeNames | SxNativeNames | MdNativeNames | string | PGNativeNames | BzNativeNames | KMNativeNames | BfNativeNames | FoNativeNames | InNativeNames | IDNativeNames | AtNativeNames | TjNativeNames | CDNativeNames | LtNativeNames | NzNativeNames | CNNativeNames | { [key: string]: Cat } | UzNativeNames | IsNativeNames | SkNativeNames |MkNativeNames | DeNativeNames | GrNativeNames | LVNativeNames | JpNativeNames | MeNativeNames | VuNativeNames | PhNativeNames;
  currencies:     BWCurrencies|KzCurrencies|FkCurrencies|KgCurrencies|KrCurrencies|GtCurrencies|ZmCurrencies|DzCurrencies|MzCurrencies|AECurrencies|GmCurrencies|MuCurrencies|EgCurrencies|JmCurrencies|JoCurrencies|KpCurrencies|TzCurrencies|SbCurrencies|MyCurrencies|ClCurrencies|DkCurrencies|NICurrencies|StCurrencies|NgCurrencies|ZaCurrencies|DjCurrencies|BoCurrencies|DkCurrencies|KyCurrencies|HuCurrencies|SyCurrencies|MwCurrencies|ArCurrencies|HTCurrencies|ScCurrencies|NaCurrencies|MoCurrencies|CRCurrencies|MgCurrencies|PlCurrencies|LkCurrencies|RuCurrencies|MaCurrencies|KwCurrencies|HrCurrencies|ByCurrencies|BdCurrencies|EtCurrencies|LaCurrencies|WsCurrencies|HnCurrencies|CzCurrencies|UaCurrencies|SrCurrencies|IRCurrencies|UyCurrencies|RwCurrencies|UgCurrencies|SzCurrencies|AdCurrencies | MmCurrencies| AFCurrencies |IqCurrencies|CFCurrencies| ThCurrencies | ToCurrencies | CACurrencies | GeCurrencies | NcCurrencies | FjCurrencies | AmCurrencies | DoCurrencies | { [key: string]: Eur } | TmCurrencies | NPCurrencies | MvCurrencies | LyCurrencies | BrCurrencies | SDCurrencies | BbCurrencies | BICurrencies | MXCurrencies | CuCurrencies | RsCurrencies | GyCurrencies | GsCurrencies | BaCurrencies | PkCurrencies | KeCurrencies | AsCurrencies | SoCurrencies | BvCurrencies | SlCurrencies | AzCurrencies | PECurrencies | AoCurrencies | CFCurrencies | SsCurrencies | TrCurrencies | AgCurrencies | AwCurrencies | TwCurrencies | SECurrencies | LrCurrencies | VeCurrencies | BmCurrencies | AlCurrencies | HkCurrencies | ErCurrencies | CoCurrencies | MnCurrencies | YeCurrencies | LBCurrencies | VnCurrencies | JeCurrencies | TtCurrencies | ILCurrencies | BgCurrencies | GiCurrencies | SgCurrencies | CwCurrencies | SaCurrencies | GhCurrencies | MdCurrencies | AqCurrencies | GBCurrencies | PGCurrencies | BzCurrencies | KMCurrencies | BfCurrencies | GnCurrencies | InCurrencies | TnCurrencies | BrCurrencies | IDCurrencies | TjCurrencies | CDCurrencies | JpCurrencies | RoCurrencies | QACurrencies | NuCurrencies | CNCurrencies | AuCurrencies | MrCurrencies | UzCurrencies | IsCurrencies | OmCurrencies | MkCurrencies | ChCurrencies | PyCurrencies | BhCurrencies | VuCurrencies | ZwCurrencies | PhCurrencies | CvCurrencies;  
  languages:      BWLanguages|KzLanguages|AELanguages|AgLanguages|MuLanguages|KhLanguages|JmLanguages|NoLanguages|XkLanguages|KpLanguages|KeLanguages|MyLanguages|ClLanguages|GlLanguages|ZaLanguages|DjLanguages|BoLanguages|DkLanguages|HuLanguages|MwLanguages|ArLanguages|HTLanguages|ScLanguages|NaLanguages|MoLanguages|ClLanguages|MgLanguages|PlLanguages|LkLanguages|EeLanguages|RuLanguages|MaLanguages|HrLanguages|ByLanguages|BdLanguages|EtLanguages|LaLanguages|CzLanguages|UaLanguages|IRLanguages|SiLanguages|RwLanguages|KeLanguages|SzLanguages|AdLanguages | MmLanguages | AFLanguages |IqLanguages|CFLanguages| ThLanguages | BeLanguages | ToLanguages | CALanguages | GeLanguages | BfLanguages | FjLanguages | NlLanguages | AmLanguages | ClLanguages | GgLanguages | TmLanguages | NPLanguages | MvLanguages | AELanguages | AoLanguages | SDLanguages | AgLanguages | RsLanguages | BTLanguages | BaLanguages | BnLanguages | PkLanguages | KeLanguages | SoLanguages | BvLanguages | AzLanguages | BoLanguages | AoLanguages | CGLanguages | TrLanguages | AgLanguages | AwLanguages | CNLanguages | AxLanguages | AlLanguages | HkLanguages | ErLanguages | MnLanguages | DjLanguages | VnLanguages | JeLanguages | ILLanguages | BgLanguages | ItLanguages | SgLanguages | SxLanguages | MdLanguages | string | PGLanguages | BzLanguages | KMLanguages | BfLanguages | FoLanguages | InLanguages | IDLanguages | AtLanguages | TjLanguages | CDLanguages | LtLanguages | NzLanguages | CNLanguages | NfLanguages | UzLanguages | IsLanguages | MkLanguages | DeLanguages | GrLanguages | ArLanguages | LVLanguages | JpLanguages | MeLanguages | VuLanguages | ZwLanguages | PhLanguages | SkLanguages; 
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string| AFRegionalBloc[] | AERegionalBloc[];
}

export interface Ad {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AdNativeNames;
  currencies:     AdCurrencies;
  languages:      AdLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface AdCurrencies {
  EUR: Eur;
}

export interface Eur {
  name:   string;
  symbol: string;
}

export interface AdDemonyms {
  eng: EngClass;
  fra: EngClass;
}

export interface EngClass {
  f: string;
  m: string;
}

export interface Flag {
  small:  string;
  medium: string;
  large:  string;
}

export interface AdLanguages {
  cat: string;
}

export interface AdLatLng {
  country: number[];
  capital: number[];
}

export interface AdNativeNames {
  cat: Cat;
}

export interface Cat {
  official: string;
  common:   string;
}

export interface Translations {
  ara:  string;
  ces:  string;
  cym:  string;
  deu:  string;
  est:  string;
  fin:  string;
  fra:  string;
  hrv?: string;
  hun:  string;
  ita:  string;
  jpn?: string;
  kor:  string;
  nld:  string;
  per?: string;
  pol:  string;
  por:  string;
  rus:  string;
  slk:  string;
  spa:  string;
  swe:  string;
  urd:  string;
  zho?: string;
}

export interface Ae {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AENativeNames | AFNativeNames;
  currencies:     AECurrencies;
  languages:      AELanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface AECurrencies {
  AED: Eur;
}

export interface AEGini {
  "2018": number;
}

export interface AELanguages {
  ara: Ara;
}

export enum Ara {
  Arabic = "Arabic",
}

export interface AENativeNames {
  ara: Cat;
}

export interface AERegionalBloc {
  acronym:        Alpha2Code;
  name:           PurpleName;
  otherNames?:    OtherName[];
  otherAcronyms?: OtherAcronym[];
}

export enum Alpha2Code {
  Al = "AL",
  Au = "AU",
  Cais = "CAIS",
  Caricom = "CARICOM",
  Eeu = "EEU",
  Eu = "EU",
  Nafta = "NAFTA",
  Pa = "PA",
  Usan = "USAN",
}

export enum PurpleName {
  AfricanUnion = "African Union",
  ArabLeague = "Arab League",
  CaribbeanCommunity = "Caribbean Community",
  CentralAmericanIntegrationSystem = "Central American Integration System",
  EurasianEconomicUnion = "Eurasian Economic Union",
  EuropeanUnion = "European Union",
  NorthAmericanFreeTradeAgreement = "North American Free Trade Agreement",
  PacificAlliance = "Pacific Alliance",
  UnionOfSouthAmericanNations = "Union of South American Nations",
}

export enum OtherAcronym {
  Eaeu = "EAEU",
  Sica = "SICA",
  Unasul = "UNASUL",
  Unasur = "UNASUR",
  Uzan = "UZAN",
}

export enum OtherName {
  AccordDeLibreÉchangeNordAméricain = "Accord de Libre-échange Nord-Américain",
  AlianzaDelPacífico = "Alianza del Pacífico",
  CaribischeGemeenschap = "Caribische Gemeenschap",
  CommunautéCaribéenne = "Communauté Caribéenne",
  ComunidadDelCaribe = "Comunidad del Caribe",
  JāmiAtAdDuwalAlArabīyah = "Jāmiʻat ad-Duwal al-ʻArabīyah",
  LeagueOfArabStates = "League of Arab States",
  SistemaDeLaIntegraciónCentroamericana = "Sistema de la Integración Centroamericana,",
  SouthAmericanUnion = "South American Union",
  TratadoDeLibreComercioDeAméricaDelNorte = "Tratado de Libre Comercio de América del Norte",
  UmojaWaAfrika = "Umoja wa Afrika",
  UnieVanZuidAmerikaanseNaties = "Unie van Zuid-Amerikaanse Naties",
  UnionAfricaine = "Union africaine",
  UniãoAfricana = "União Africana",
  UniãoDeNaçõesSulAmericanas = "União de Nações Sul-Americanas",
  UniónAfricana = "Unión Africana",
  UniónDeNacionesSuramericanas = "Unión de Naciones Suramericanas",
  الاتحادالأفريقي = "الاتحاد الأفريقي",
  جامعةالدولالعربية = "جامعة الدول العربية",
}

export interface Af {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AFNativeNames;
  currencies:     AFCurrencies;
  languages:      AFLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface AFCurrencies {
  AFN: Eur;
}

export interface AFLanguages {
  prs: string;
  pus: string;
  tuk: string;
}

export interface AFNativeNames {
  prs: Cat;
  pus: Cat;
  tuk: Cat;
}

export interface AFRegionalBloc {
  acronym: Acronym;
  name:    FluffyName;
}

export enum Acronym {
  Asean = "ASEAN",
  Cefta = "CEFTA",
  Efta = "EFTA",
  Eu = "EU",
  Saarc = "SAARC",
}

export enum FluffyName {
  AssociationOfSoutheastAsianNations = "Association of Southeast Asian Nations",
  CentralEuropeanFreeTradeAgreement = "Central European Free Trade Agreement",
  EuropeanFreeTradeAssociation = "European Free Trade Association",
  EuropeanUnion = "European Union",
  SouthAsianAssociationForRegionalCooperation = "South Asian Association for Regional Cooperation",
}

export interface Ag {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    AgNativeNames;
  currencies:     AgCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AgRegionalBloc[];
}

export interface AgCurrencies {
  XCD: Eur;
}

export interface AgLanguages {
  eng: EngEnum;
}

export enum EngEnum {
  English = "English",
}

export interface AgNativeNames {
  eng: Cat;
}

export interface AgRegionalBloc {
  acronym:    Alpha2Code;
  name:       PurpleName;
  otherNames: OtherName[];
}

export interface AI {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    AgNativeNames;
  currencies:     AgCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface Al {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     Alpha2Code;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AlGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AlNativeNames;
  currencies:     AlCurrencies;
  languages:      AlLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface AlCurrencies {
  ALL: Eur;
}

export interface AlGini {
  "2017": number;
}

export interface AlLanguages {
  sqi: string;
}

export interface AlNativeNames {
  sqi: Cat;
}

export interface Am {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AmGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AmNativeNames;
  currencies:     AmCurrencies;
  languages:      AmLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface AmCurrencies {
  AMD: Eur;
}

export interface AmGini {
  "2019": number;
}

export interface AmLanguages {
  hye: string;
}

export interface AmNativeNames {
  hye: Cat;
}

export interface Ao {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AoNativeNames;
  currencies:     AoCurrencies;
  languages:      AoLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface AoCurrencies {
  AOA: Eur;
}

export interface AoLanguages {
  por: string;
}

export interface AoNativeNames {
  por: Cat;
}

export interface Aq {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AqLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    string;
  currencies:     AqCurrencies;
  languages:      string;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface AqCurrencies {
  AAD: Eur;
}

export interface AqLatLng {
  country: number[];
  capital: string;
}

export interface Ar {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AmGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    ArNativeNames;
  currencies:     ArCurrencies;
  languages:      ArLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface ArCurrencies {
  ARS: Eur;
}

export interface ArLanguages {
  grn: string;
  spa: SPA;
}

export enum SPA {
  Spanish = "Spanish",
}

export interface ArNativeNames {
  grn: Cat;
  spa: Cat;
}

export interface As {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    AsNativeNames;
  currencies:     AsCurrencies;
  languages:      AsLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface AsCurrencies {
  USD: Eur;
}

export interface AsLanguages {
  eng: EngEnum;
  smo: string;
}

export interface AsNativeNames {
  eng: Cat;
  smo: Cat;
}

export interface At {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AtNativeNames;
  currencies:     AdCurrencies;
  languages:      AtLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface AtLanguages {
  bar: string;
}

export interface AtNativeNames {
  bar: Cat;
}

export interface Au {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     Alpha2Code;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   Alpha2Code[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AuGini;
  timezones:      string[];
  borders:        string;
  nativeNames:    AgNativeNames;
  currencies:     AuCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface AuCurrencies {
  AUD: Eur;
}

export interface AuGini {
  "2014": number;
}

export interface Aw {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    { [key: string]: Cat };
  currencies:     AwCurrencies;
  languages:      AwLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface AwCurrencies {
  AWG: Eur;
}

export interface AwLanguages {
  nld: string;
  pap: string;
}

export interface Ax {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    AxNativeNames;
  currencies:     AdCurrencies;
  languages:      AxLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface AxLanguages {
  swe: string;
}

export interface AxNativeNames {
  swe: Cat;
}

export interface Az {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AzGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AzNativeNames;
  currencies:     AzCurrencies;
  languages:      AzLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface AzCurrencies {
  AZN: Eur;
}

export interface AzGini {
  "2005": number;
}

export interface AzLanguages {
  aze: string;
  rus: string;
}

export interface AzNativeNames {
  aze: Cat;
  rus: Cat;
}

export interface Ba {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BaGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    { [key: string]: Cat };
  currencies:     BaCurrencies;
  languages:      BaLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface BaCurrencies {
  BAM: BAM;
}

export interface BAM {
  name: string;
}

export interface BaGini {
  "2011": number;
}

export interface BaLanguages {
  bos: string;
  hrv: string;
  srp: string;
}

export interface Bb {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    AgNativeNames;
  currencies:     BbCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface BbCurrencies {
  BBD: Eur;
}

export interface Bd {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BdGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    BdNativeNames;
  currencies:     BdCurrencies;
  languages:      BdLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface BdCurrencies {
  BDT: Eur;
}

export interface BdGini {
  "2016": number;
}

export interface BdLanguages {
  ben: string;
}

export interface BdNativeNames {
  ben: Cat;
}

export interface Be {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    BeNativeNames;
  currencies:     AdCurrencies;
  languages:      BeLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface BeLanguages {
  deu: string;
  fra: Fra;
  nld: string;
}

export enum Fra {
  French = "French",
}

export interface BeNativeNames {
  deu: Cat;
  fra: Cat;
  nld: Cat;
}

export interface Bf {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AuGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    BfNativeNames;
  currencies:     BfCurrencies;
  languages:      BfLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AgRegionalBloc[];
}

export interface BfCurrencies {
  XOF: Eur;
}

export interface BfLanguages {
  fra: Fra;
}

export interface BfNativeNames {
  fra: Cat;
}

export interface Bg {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    BgNativeNames;
  currencies:     BgCurrencies;
  languages:      BgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface BgCurrencies {
  BGN: Eur;
}

export interface BgLanguages {
  bul: string;
}

export interface BgNativeNames {
  bul: Cat;
}

export interface Bh {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    AENativeNames;
  currencies:     BhCurrencies;
  languages:      AELanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface BhCurrencies {
  BHD: Eur;
}

export interface Bi {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BIGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    BINativeNames;
  currencies:     BICurrencies;
  languages:      BILanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface BICurrencies {
  BIF: Eur;
}

export interface BIGini {
  "2013": number;
}

export interface BILanguages {
  fra: Fra;
  run: string;
}

export interface BINativeNames {
  fra: Cat;
  run: Cat;
}

export interface Bj {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BjGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    BfNativeNames;
  currencies:     BfCurrencies;
  languages:      BfLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AgRegionalBloc[];
}

export interface BjGini {
  "2015": number;
}

export interface Bl {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    BfNativeNames;
  currencies:     AdCurrencies;
  languages:      BfLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface Bm {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    AgNativeNames;
  currencies:     BmCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface BmCurrencies {
  BMD: Eur;
}

export interface Bn {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string[];
  nativeNames:    BnNativeNames;
  currencies:     { [key: string]: Eur };
  languages:      BnLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface BnLanguages {
  msa: string;
}

export interface BnNativeNames {
  msa: Cat;
}

export interface Bo {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AmGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    BoNativeNames;
  currencies:     BoCurrencies;
  languages:      BoLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface BoCurrencies {
  BOB: Eur;
}

export interface BoLanguages {
  aym:  string;
  grn?: string;
  que:  string;
  spa:  SPA;
}

export interface BoNativeNames {
  aym:  Cat;
  grn?: Cat;
  que:  Cat;
  spa:  Cat;
}

export interface Bq {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    { [key: string]: Cat };
  currencies:     AsCurrencies;
  languages:      BqLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface BqLanguages {
  eng: EngEnum;
  nld: string;
  pap: string;
}

export interface Br {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AmGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AoNativeNames;
  currencies:     BrCurrencies;
  languages:      AoLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface BrCurrencies {
  BRL: Eur;
}

export interface Bs {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    AgNativeNames;
  currencies:     BsCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface BsCurrencies {
  BSD: Eur;
  USD: Eur;
}

export interface Bt {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AlGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    BTNativeNames;
  currencies:     { [key: string]: Eur };
  languages:      BTLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface BTLanguages {
  dzo: string;
}

export interface BTNativeNames {
  dzo: Cat;
}

export interface Bv {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AqLatLng;
  demonyms:       string;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    BvNativeNames;
  currencies:     BvCurrencies;
  languages:      BvLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface BvCurrencies {
  NOK: Eur;
}

export interface BvLanguages {
  nor: string;
}

export interface BvNativeNames {
  nor: Cat;
}

export interface Bw {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BjGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    BWNativeNames;
  currencies:     BWCurrencies;
  languages:      BWLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface BWCurrencies {
  BWP: Eur;
}

export interface BWLanguages {
  eng: EngEnum;
  tsn: string;
}

export interface BWNativeNames {
  eng: Cat;
  tsn: Cat;
}

export interface By {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AmGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    ByNativeNames;
  currencies:     ByCurrencies;
  languages:      ByLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface ByCurrencies {
  BYN: Eur;
}

export interface ByLanguages {
  bel: string;
  rus: string;
}

export interface ByNativeNames {
  bel: Cat;
  rus: Cat;
}

export interface Bz {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BzGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    BzNativeNames;
  currencies:     BzCurrencies;
  languages:      BzLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface BzCurrencies {
  BZD: Eur;
}

export interface BzGini {
  "1999": number;
}

export interface BzLanguages {
  bjz: string;
  eng: EngEnum;
  spa: SPA;
}

export interface BzNativeNames {
  bjz: Cat;
  eng: Cat;
  spa: Cat;
}

export interface Ca {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AlGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    CANativeNames;
  currencies:     CACurrencies;
  languages:      CALanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface CACurrencies {
  CAD: Eur;
}

export interface CALanguages {
  eng: EngEnum;
  fra: Fra;
}

export interface CANativeNames {
  eng: Cat;
  fra: Cat;
}

export interface Cc {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       CcDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    AgNativeNames;
  currencies:     AuCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface CcDemonyms {
  eng: EngClass;
}

export interface Cd {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           CDGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    CDNativeNames;
  currencies:     CDCurrencies;
  languages:      CDLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface CDCurrencies {
  CDF: Eur;
}

export interface CDGini {
  "2012": number;
}

export interface CDLanguages {
  fra: Fra;
  kon: string;
  lin: string;
  lua: string;
  swa: string;
}

export interface CDNativeNames {
  fra: Cat;
  kon: Cat;
  lin: Cat;
  lua: Cat;
  swa: Cat;
}

export interface Cf {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           CFGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    CFNativeNames;
  currencies:     CFCurrencies;
  languages:      CFLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface CFCurrencies {
  XAF: Eur;
}

export interface CFGini {
  "2008": number;
}

export interface CFLanguages {
  fra: Fra;
  sag: string;
}

export interface CFNativeNames {
  fra: Cat;
  sag: Cat;
}

export interface Cg {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BaGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    CGNativeNames;
  currencies:     CFCurrencies;
  languages:      CGLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface CGLanguages {
  fra: Fra;
  kon: string;
  lin: string;
}

export interface CGNativeNames {
  fra: Cat;
  kon: Cat;
  lin: Cat;
}

export interface Ch {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    ChNativeNames;
  currencies:     ChCurrencies;
  languages:      ChLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface ChCurrencies {
  CHF: Eur;
}

export interface ChLanguages {
  fra: Fra;
  gsw: string;
  ita: string;
  roh: string;
}

export interface ChNativeNames {
  fra: Cat;
  gsw: Cat;
  ita: Cat;
  roh: Cat;
}

export interface Ck {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    CkNativeNames;
  currencies:     { [key: string]: Eur };
  languages:      CkLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface CkLanguages {
  eng: EngEnum;
  rar: string;
}

export interface CkNativeNames {
  eng: Cat;
  rar: Cat;
}

export interface Cl {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AlGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    ClNativeNames;
  currencies:     ClCurrencies;
  languages:      ClLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface ClCurrencies {
  CLP: Eur;
}

export interface ClLanguages {
  spa: SPA;
}

export interface ClNativeNames {
  spa: Cat;
}

export interface Cm {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AuGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    CANativeNames;
  currencies:     CFCurrencies;
  languages:      CALanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface Cn {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BdGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    CNNativeNames;
  currencies:     CNCurrencies;
  languages:      CNLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface CNCurrencies {
  CNY: Eur;
}

export interface CNLanguages {
  zho: string;
}

export interface CNNativeNames {
  zho: Cat;
}

export interface Co {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AmGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    ClNativeNames;
  currencies:     CoCurrencies;
  languages:      ClLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface CoCurrencies {
  COP: Eur;
}

export interface Cr {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AmGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    ClNativeNames;
  currencies:     CRCurrencies;
  languages:      ClLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface CRCurrencies {
  CRC: Eur;
}

export interface Cu {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    ClNativeNames;
  currencies:     CuCurrencies;
  languages:      ClLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface CuCurrencies {
  CUC: Eur;
  CUP: Eur;
}

export interface Cv {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BjGini;
  timezones:      string[];
  borders:        string;
  nativeNames:    AoNativeNames;
  currencies:     CvCurrencies;
  languages:      AoLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface CvCurrencies {
  CVE: Eur;
}

export interface Cw {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    CwNativeNames;
  currencies:     CwCurrencies;
  languages:      BqLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface CwCurrencies {
  ANG: Eur;
}

export interface CwNativeNames {
  eng: Cat;
  nld: Cat;
  pap: Cat;
}

export interface Cy {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string;
  nativeNames:    CyNativeNames;
  currencies:     AdCurrencies;
  languages:      CyLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface CyLanguages {
  ell: string;
  tur: string;
}

export interface CyNativeNames {
  ell: Cat;
  tur: Cat;
}

export interface Cz {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    { [key: string]: Cat };
  currencies:     CzCurrencies;
  languages:      CzLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface CzCurrencies {
  CZK: Eur;
}

export interface CzLanguages {
  ces: string;
  slk: string;
}

export interface De {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BdGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    DeNativeNames;
  currencies:     AdCurrencies;
  languages:      DeLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface DeLanguages {
  deu: string;
}

export interface DeNativeNames {
  deu: Cat;
}

export interface Dj {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AlGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    DjNativeNames;
  currencies:     DjCurrencies;
  languages:      DjLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface DjCurrencies {
  DJF: Eur;
}

export interface DjLanguages {
  ara: Ara;
  fra: Fra;
}

export interface DjNativeNames {
  ara: Cat;
  fra: Cat;
}

export interface Dk {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    DkNativeNames;
  currencies:     DkCurrencies;
  languages:      DkLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface DkCurrencies {
  DKK: Eur;
}

export interface DkLanguages {
  dan: string;
}

export interface DkNativeNames {
  dan: Cat;
}

export interface Do {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AmGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    ClNativeNames;
  currencies:     DoCurrencies;
  languages:      ClLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface DoCurrencies {
  DOP: Eur;
}

export interface Dz {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BaGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AENativeNames;
  currencies:     DzCurrencies;
  languages:      AELanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface DzCurrencies {
  DZD: Eur;
}

export interface Ec {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AmGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    ClNativeNames;
  currencies:     AsCurrencies;
  languages:      ClLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface Ee {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    EeNativeNames;
  currencies:     AdCurrencies;
  languages:      EeLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface EeLanguages {
  est: string;
}

export interface EeNativeNames {
  est: Cat;
}

export interface Eg {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AlGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AENativeNames;
  currencies:     EgCurrencies;
  languages:      AELanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface EgCurrencies {
  EGP: Eur;
}

export interface Eh {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       CcDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string[];
  nativeNames:    EhNativeNames;
  currencies:     EhCurrencies;
  languages:      EhLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface EhCurrencies {
  DZD: Eur;
  MAD: Eur;
  MRU: Eur;
}

export interface EhLanguages {
  ber: string;
  mey: string;
  spa: SPA;
}

export interface EhNativeNames {
  ber: Cat;
  mey: Cat;
  spa: Cat;
}

export interface Er {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string[];
  nativeNames:    ErNativeNames;
  currencies:     ErCurrencies;
  languages:      ErLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface ErCurrencies {
  ERN: Eur;
}

export interface ErLanguages {
  ara: Ara;
  eng: EngEnum;
  tir: string;
}

export interface ErNativeNames {
  ara: Cat;
  eng: Cat;
  tir: Cat;
}

export interface Es {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    ClNativeNames;
  currencies:     AdCurrencies;
  languages:      ClLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface Et {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BjGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    EtNativeNames;
  currencies:     EtCurrencies;
  languages:      EtLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface EtCurrencies {
  ETB: Eur;
}

export interface EtLanguages {
  amh: string;
}

export interface EtNativeNames {
  amh: Cat;
}

export interface Fi {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    FiNativeNames;
  currencies:     AdCurrencies;
  languages:      FiLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface FiLanguages {
  fin: string;
  swe: string;
}

export interface FiNativeNames {
  fin: Cat;
  swe: Cat;
}

export interface Fj {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BIGini;
  timezones:      string[];
  borders:        string;
  nativeNames:    FjNativeNames;
  currencies:     FjCurrencies;
  languages:      FjLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface FjCurrencies {
  FJD: Eur;
}

export interface FjLanguages {
  eng: EngEnum;
  fij: string;
  hif: string;
}

export interface FjNativeNames {
  eng: Cat;
  fij: Cat;
  hif: Cat;
}

export interface Fk {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    AgNativeNames;
  currencies:     FkCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface FkCurrencies {
  FKP: Eur;
}

export interface Fm {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BIGini;
  timezones:      string[];
  borders:        string;
  nativeNames:    AgNativeNames;
  currencies:     AsCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface Fo {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    FoNativeNames;
  currencies:     { [key: string]: Eur };
  languages:      FoLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface FoLanguages {
  dan: string;
  fao: string;
}

export interface FoNativeNames {
  dan: Cat;
  fao: Cat;
}

export interface Fr {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    BfNativeNames;
  currencies:     AdCurrencies;
  languages:      BfLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface Ga {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AlGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    BfNativeNames;
  currencies:     CFCurrencies;
  languages:      BfLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface Gb {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AlGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AgNativeNames;
  currencies:     GBCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface GBCurrencies {
  GBP: Eur;
}

export interface Ge {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AmGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    GeNativeNames;
  currencies:     GeCurrencies;
  languages:      GeLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface GeCurrencies {
  GEL: Eur;
}

export interface GeLanguages {
  kat: string;
}

export interface GeNativeNames {
  kat: Cat;
}

export interface Gf {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string[];
  nativeNames:    BfNativeNames;
  currencies:     AdCurrencies;
  languages:      BfLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface Gg {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    GgNativeNames;
  currencies:     { [key: string]: Eur };
  languages:      GgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface GgLanguages {
  eng: EngEnum;
  fra: Fra;
  nfr: string;
}

export interface GgNativeNames {
  eng: Cat;
  fra: Cat;
  nfr: Cat;
}

export interface Gh {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BdGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AgNativeNames;
  currencies:     GhCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface GhCurrencies {
  GHS: Eur;
}

export interface Gi {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AgNativeNames;
  currencies:     GiCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface GiCurrencies {
  GIP: Eur;
}

export interface Gl {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    GlNativeNames;
  currencies:     DkCurrencies;
  languages:      GlLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface GlLanguages {
  kal: string;
}

export interface GlNativeNames {
  kal: Cat;
}

export interface Gm {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BjGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AgNativeNames;
  currencies:     GmCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface GmCurrencies {
  GMD: Eur;
}

export interface Gn {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           CDGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    BfNativeNames;
  currencies:     GnCurrencies;
  languages:      BfLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface GnCurrencies {
  GNF: Eur;
}

export interface Gq {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string[];
  nativeNames:    GqNativeNames;
  currencies:     CFCurrencies;
  languages:      GqLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface GqLanguages {
  fra: Fra;
  por: string;
  spa: SPA;
}

export interface GqNativeNames {
  fra: Cat;
  por: Cat;
  spa: Cat;
}

export interface Gr {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    GrNativeNames;
  currencies:     AdCurrencies;
  languages:      GrLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface GrLanguages {
  ell: string;
}

export interface GrNativeNames {
  ell: Cat;
}

export interface Gs {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       CcDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    AgNativeNames;
  currencies:     GsCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface GsCurrencies {
  SHP: Eur;
}

export interface Gt {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AuGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    ClNativeNames;
  currencies:     GtCurrencies;
  languages:      ClLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface GtCurrencies {
  GTQ: Eur;
}

export interface Gu {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       CcDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    GuNativeNames;
  currencies:     AsCurrencies;
  languages:      GuLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface GuLanguages {
  cha: string;
  eng: EngEnum;
  spa: SPA;
}

export interface GuNativeNames {
  cha: Cat;
  eng: Cat;
  spa: Cat;
}

export interface Gw {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           GwGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    GwNativeNames;
  currencies:     BfCurrencies;
  languages:      GwLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface GwGini {
  "2010": number;
}

export interface GwLanguages {
  por: string;
  pov: string;
}

export interface GwNativeNames {
  por: Cat;
  pov: Cat;
}

export interface Gy {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           GyGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AgNativeNames;
  currencies:     GyCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface GyCurrencies {
  GYD: Eur;
}

export interface GyGini {
  "1998": number;
}

export interface Hk {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string[];
  nativeNames:    HkNativeNames;
  currencies:     HkCurrencies;
  languages:      HkLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface HkCurrencies {
  HKD: Eur;
}

export interface HkLanguages {
  eng: EngEnum;
  zho: string;
}

export interface HkNativeNames {
  eng: Cat;
  zho: Cat;
}

export interface Hm {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AqLatLng;
  demonyms:       CcDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    AgNativeNames;
  currencies:     AuCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface Hn {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AmGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    ClNativeNames;
  currencies:     HnCurrencies;
  languages:      ClLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface HnCurrencies {
  HNL: Eur;
}

export interface Hr {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    HrNativeNames;
  currencies:     HrCurrencies;
  languages:      HrLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface HrCurrencies {
  HRK: Eur;
}

export interface HrLanguages {
  hrv: string;
}

export interface HrNativeNames {
  hrv: Cat;
}

export interface HT {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           CDGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    HTNativeNames;
  currencies:     HTCurrencies;
  languages:      HTLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface HTCurrencies {
  HTG: Eur;
}

export interface HTLanguages {
  fra: Fra;
  hat: string;
}

export interface HTNativeNames {
  fra: Cat;
  hat: Cat;
}

export interface Hu {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    HuNativeNames;
  currencies:     HuCurrencies;
  languages:      HuLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface HuCurrencies {
  HUF: Eur;
}

export interface HuLanguages {
  hun: string;
}

export interface HuNativeNames {
  hun: Cat;
}

export interface Id {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AmGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    IDNativeNames;
  currencies:     IDCurrencies;
  languages:      IDLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface IDCurrencies {
  IDR: Eur;
}

export interface IDLanguages {
  ind: string;
}

export interface IDNativeNames {
  ind: Cat;
}

export interface Ie {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AlGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    IeNativeNames;
  currencies:     AdCurrencies;
  languages:      IeLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface IeLanguages {
  eng: EngEnum;
  gle: string;
}

export interface IeNativeNames {
  eng: Cat;
  gle: Cat;
}

export interface Il {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BdGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    ILNativeNames;
  currencies:     ILCurrencies;
  languages:      ILLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface ILCurrencies {
  ILS: Eur;
}

export interface ILLanguages {
  ara: Ara;
  heb: string;
}

export interface ILNativeNames {
  ara: Cat;
  heb: Cat;
}

export interface Im {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       CcDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    { [key: string]: Cat };
  currencies:     IMCurrencies;
  languages:      IMLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface IMCurrencies {
  GBP: Eur;
  IMP: Eur;
}

export interface IMLanguages {
  eng: EngEnum;
  glv: string;
}

export interface In {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BaGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    InNativeNames;
  currencies:     InCurrencies;
  languages:      InLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface InCurrencies {
  INR: Eur;
}

export interface InLanguages {
  eng: EngEnum;
  hin: string;
  tam: string;
}

export interface InNativeNames {
  eng: Cat;
  hin: Cat;
  tam: Cat;
}

export interface Io {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       CcDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    AgNativeNames;
  currencies:     AsCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface Iq {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           CDGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    IqNativeNames;
  currencies:     IqCurrencies;
  languages:      IqLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface IqCurrencies {
  IQD: Eur;
}

export interface IqLanguages {
  ara: Ara;
  arc: string;
  ckb: string;
}

export interface IqNativeNames {
  ara: Cat;
  arc: Cat;
  ckb: Cat;
}

export interface Ir {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    IRNativeNames;
  currencies:     IRCurrencies;
  languages:      IRLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface IRCurrencies {
  IRR: Eur;
}

export interface IRLanguages {
  fas: string;
}

export interface IRNativeNames {
  fas: Cat;
}

export interface Is {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AlGini;
  timezones:      string[];
  borders:        string;
  nativeNames:    IsNativeNames;
  currencies:     IsCurrencies;
  languages:      IsLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface IsCurrencies {
  ISK: Eur;
}

export interface IsLanguages {
  isl: string;
}

export interface IsNativeNames {
  isl: Cat;
}

export interface It {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AlGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    ItNativeNames;
  currencies:     AdCurrencies;
  languages:      ItLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface ItLanguages {
  ita: string;
}

export interface ItNativeNames {
  ita: Cat;
}

export interface Je {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    JeNativeNames;
  currencies:     JeCurrencies;
  languages:      JeLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface JeCurrencies {
  GBP: Eur;
  JEP: Eur;
}

export interface JeLanguages {
  eng: EngEnum;
  fra: Fra;
  nrf: string;
}

export interface JeNativeNames {
  eng: Cat;
  fra: Cat;
  nrf: Cat;
}

export interface Jm {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           JmGini;
  timezones:      string[];
  borders:        string;
  nativeNames:    JmNativeNames;
  currencies:     JmCurrencies;
  languages:      JmLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface JmCurrencies {
  JMD: Eur;
}

export interface JmGini {
  "2004": number;
}

export interface JmLanguages {
  eng: EngEnum;
  jam: string;
}

export interface JmNativeNames {
  eng: Cat;
  jam: Cat;
}

export interface Jo {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           GwGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AENativeNames;
  currencies:     JoCurrencies;
  languages:      AELanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface JoCurrencies {
  JOD: Eur;
}

export interface Jp {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BIGini;
  timezones:      string[];
  borders:        string;
  nativeNames:    JpNativeNames;
  currencies:     JpCurrencies;
  languages:      JpLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface JpCurrencies {
  JPY: Eur;
}

export interface JpLanguages {
  jpn: string;
}

export interface JpNativeNames {
  jpn: Cat;
}

export interface Ke {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BjGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    KeNativeNames;
  currencies:     KeCurrencies;
  languages:      KeLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface KeCurrencies {
  KES: Eur;
}

export interface KeLanguages {
  eng: EngEnum;
  swa: string;
}

export interface KeNativeNames {
  eng: Cat;
  swa: Cat;
}

export interface Kg {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AmGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    KgNativeNames;
  currencies:     KgCurrencies;
  languages:      KgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface KgCurrencies {
  KGS: Eur;
}

export interface KgLanguages {
  kir: string;
  rus: string;
}

export interface KgNativeNames {
  kir: Cat;
  rus: Cat;
}

export interface Kh {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string[];
  nativeNames:    KhNativeNames;
  currencies:     { [key: string]: Eur };
  languages:      KhLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface KhLanguages {
  khm: string;
}

export interface KhNativeNames {
  khm: Cat;
}

export interface Ki {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           KiGini;
  timezones:      string[];
  borders:        string;
  nativeNames:    KiNativeNames;
  currencies:     KiCurrencies;
  languages:      KiLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface KiCurrencies {
  AUD: Eur;
  KID: Eur;
}

export interface KiGini {
  "2006": number;
}

export interface KiLanguages {
  eng: EngEnum;
  gil: string;
}

export interface KiNativeNames {
  eng: Cat;
  gil: Cat;
}

export interface Km {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AuGini;
  timezones:      string[];
  borders:        string;
  nativeNames:    KMNativeNames;
  currencies:     KMCurrencies;
  languages:      KMLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface KMCurrencies {
  KMF: Eur;
}

export interface KMLanguages {
  ara: Ara;
  fra: Fra;
  zdj: string;
}

export interface KMNativeNames {
  ara: Cat;
  fra: Cat;
  zdj: Cat;
}

export interface Kp {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string[];
  nativeNames:    KpNativeNames;
  currencies:     KpCurrencies;
  languages:      KpLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface KpCurrencies {
  KPW: Eur;
}

export interface KpLanguages {
  kor: string;
}

export interface KpNativeNames {
  kor: Cat;
}

export interface Kr {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BdGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    KpNativeNames;
  currencies:     KrCurrencies;
  languages:      KpLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface KrCurrencies {
  KRW: Eur;
}

export interface Kw {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AENativeNames;
  currencies:     KwCurrencies;
  languages:      AELanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface KwCurrencies {
  KWD: Eur;
}

export interface Ky {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    AgNativeNames;
  currencies:     KyCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface KyCurrencies {
  KYD: Eur;
}

export interface Kz {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    KzNativeNames;
  currencies:     KzCurrencies;
  languages:      KzLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface KzCurrencies {
  KZT: Eur;
}

export interface KzLanguages {
  kaz: string;
  rus: string;
}

export interface KzNativeNames {
  kaz: Cat;
  rus: Cat;
}

export interface La {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    LaNativeNames;
  currencies:     LaCurrencies;
  languages:      LaLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface LaCurrencies {
  LAK: Eur;
}

export interface LaLanguages {
  lao: string;
}

export interface LaNativeNames {
  lao: Cat;
}

export interface Lb {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BaGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    DjNativeNames;
  currencies:     LBCurrencies;
  languages:      DjLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface LBCurrencies {
  LBP: Eur;
}

export interface Lc {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BdGini;
  timezones:      string[];
  borders:        string;
  nativeNames:    AgNativeNames;
  currencies:     AgCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface Li {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string[];
  nativeNames:    DeNativeNames;
  currencies:     ChCurrencies;
  languages:      DeLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface Lk {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BdGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    LkNativeNames;
  currencies:     LkCurrencies;
  languages:      LkLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface LkCurrencies {
  LKR: Eur;
}

export interface LkLanguages {
  sin: string;
  tam: string;
}

export interface LkNativeNames {
  sin: Cat;
  tam: Cat;
}

export interface Lr {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BdGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AgNativeNames;
  currencies:     LrCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface LrCurrencies {
  LRD: Eur;
}

export interface Ls {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AlGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    LsNativeNames;
  currencies:     LsCurrencies;
  languages:      LsLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface LsCurrencies {
  LSL: Eur;
  ZAR: Eur;
}

export interface LsLanguages {
  eng: EngEnum;
  sot: string;
}

export interface LsNativeNames {
  eng: Cat;
  sot: Cat;
}

export interface Lt {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    LtNativeNames;
  currencies:     AdCurrencies;
  languages:      LtLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface LtLanguages {
  lit: string;
}

export interface LtNativeNames {
  lit: Cat;
}

export interface Lu {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    LuNativeNames;
  currencies:     AdCurrencies;
  languages:      LuLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface LuLanguages {
  deu: string;
  fra: Fra;
  ltz: string;
}

export interface LuNativeNames {
  deu: Cat;
  fra: Cat;
  ltz: Cat;
}

export interface Lv {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    LVNativeNames;
  currencies:     AdCurrencies;
  languages:      LVLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface LVLanguages {
  lav: string;
}

export interface LVNativeNames {
  lav: Cat;
}

export interface Ly {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AENativeNames;
  currencies:     LyCurrencies;
  languages:      AELanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface LyCurrencies {
  LYD: Eur;
}

export interface Ma {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BIGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    MaNativeNames;
  currencies:     MaCurrencies;
  languages:      MaLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface MaCurrencies {
  MAD: Eur;
}

export interface MaLanguages {
  ara: Ara;
  ber: string;
}

export interface MaNativeNames {
  ara: Cat;
  ber: Cat;
}

export interface Mc {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string[];
  nativeNames:    BfNativeNames;
  currencies:     AdCurrencies;
  languages:      BfLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface Md {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    MdNativeNames;
  currencies:     MdCurrencies;
  languages:      MdLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface MdCurrencies {
  MDL: Eur;
}

export interface MdLanguages {
  ron: string;
}

export interface MdNativeNames {
  ron: Cat;
}

export interface Me {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BdGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    MeNativeNames;
  currencies:     AdCurrencies;
  languages:      MeLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface MeLanguages {
  cnr: string;
}

export interface MeNativeNames {
  cnr: Cat;
}

export interface Mg {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           CDGini;
  timezones:      string[];
  borders:        string;
  nativeNames:    MgNativeNames;
  currencies:     MgCurrencies;
  languages:      MgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface MgCurrencies {
  MGA: Eur;
}

export interface MgLanguages {
  fra: Fra;
  mlg: string;
}

export interface MgNativeNames {
  fra: Cat;
  mlg: Cat;
}

export interface Mh {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    MhNativeNames;
  currencies:     AsCurrencies;
  languages:      MhLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface MhLanguages {
  eng: EngEnum;
  mah: string;
}

export interface MhNativeNames {
  eng: Cat;
  mah: Cat;
}

export interface Mk {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    MkNativeNames;
  currencies:     MkCurrencies;
  languages:      MkLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface MkCurrencies {
  MKD: Eur;
}

export interface MkLanguages {
  mkd: string;
}

export interface MkNativeNames {
  mkd: Cat;
}

export interface Ml {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           MlGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    BfNativeNames;
  currencies:     BfCurrencies;
  languages:      BfLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface MlGini {
  "2009": number;
}

export interface Mm {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AlGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    MmNativeNames;
  currencies:     MmCurrencies;
  languages:      MmLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface MmCurrencies {
  MMK: Eur;
}

export interface MmLanguages {
  mya: string;
}

export interface MmNativeNames {
  mya: Cat;
}

export interface Mn {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    MnNativeNames;
  currencies:     MnCurrencies;
  languages:      MnLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface MnCurrencies {
  MNT: Eur;
}

export interface MnLanguages {
  mon: string;
}

export interface MnNativeNames {
  mon: Cat;
}

export interface Mo {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AqLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string[];
  nativeNames:    MoNativeNames;
  currencies:     MoCurrencies;
  languages:      MoLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface MoCurrencies {
  MOP: Eur;
}

export interface MoLanguages {
  por: string;
  zho: string;
}

export interface MoNativeNames {
  por: Cat;
  zho: Cat;
}

export interface Mp {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    MpNativeNames;
  currencies:     AsCurrencies;
  languages:      MpLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface MpLanguages {
  cal: string;
  cha: string;
  eng: EngEnum;
}

export interface MpNativeNames {
  cal: Cat;
  cha: Cat;
  eng: Cat;
}

export interface Mr {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AuGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AENativeNames;
  currencies:     MrCurrencies;
  languages:      AELanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface MrCurrencies {
  MRU: Eur;
}

export interface Mt {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string;
  nativeNames:    { [key: string]: Cat };
  currencies:     AdCurrencies;
  languages:      MTLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface MTLanguages {
  eng: EngEnum;
  mlt: string;
}

export interface Mu {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AlGini;
  timezones:      string[];
  borders:        string;
  nativeNames:    MuNativeNames;
  currencies:     MuCurrencies;
  languages:      MuLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface MuCurrencies {
  MUR: Eur;
}

export interface MuLanguages {
  eng: EngEnum;
  fra: Fra;
  mfe: string;
}

export interface MuNativeNames {
  eng: Cat;
  fra: Cat;
  mfe: Cat;
}

export interface Mv {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BdGini;
  timezones:      string[];
  borders:        string;
  nativeNames:    MvNativeNames;
  currencies:     MvCurrencies;
  languages:      MvLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface MvCurrencies {
  MVR: Eur;
}

export interface MvLanguages {
  div: string;
}

export interface MvNativeNames {
  div: Cat;
}

export interface Mw {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BdGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    MwNativeNames;
  currencies:     MwCurrencies;
  languages:      MwLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface MwCurrencies {
  MWK: Eur;
}

export interface MwLanguages {
  eng: EngEnum;
  nya: string;
}

export interface MwNativeNames {
  eng: Cat;
  nya: Cat;
}

export interface Mx {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    ClNativeNames;
  currencies:     MXCurrencies;
  languages:      ClLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface MXCurrencies {
  MXN: Eur;
}

export interface My {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BjGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    MyNativeNames;
  currencies:     MyCurrencies;
  languages:      MyLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface MyCurrencies {
  MYR: Eur;
}

export interface MyLanguages {
  eng: EngEnum;
  msa: string;
}

export interface MyNativeNames {
  eng: Cat;
  msa: Cat;
}

export interface Mz {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AuGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AoNativeNames;
  currencies:     MzCurrencies;
  languages:      AoLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface MzCurrencies {
  MZN: Eur;
}

export interface Na {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BjGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    { [key: string]: Cat };
  currencies:     NaCurrencies;
  languages:      NaLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface NaCurrencies {
  NAD: Eur;
  ZAR: Eur;
}

export interface NaLanguages {
  afr: string;
  deu: string;
  eng: EngEnum;
  her: string;
  hgm: string;
  kwn: string;
  loz: string;
  ndo: string;
  tsn: string;
}

export interface Nc {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    BfNativeNames;
  currencies:     NcCurrencies;
  languages:      BfLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface NcCurrencies {
  XPF: Eur;
}

export interface Nf {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    { [key: string]: Cat };
  currencies:     AuCurrencies;
  languages:      NfLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface NfLanguages {
  eng: EngEnum;
  pih: string;
}

export interface Ng {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AgNativeNames;
  currencies:     NgCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface NgCurrencies {
  NGN: Eur;
}

export interface Ni {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AuGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    ClNativeNames;
  currencies:     NICurrencies;
  languages:      ClLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface NICurrencies {
  NIO: Eur;
}

export interface Nl {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    NlNativeNames;
  currencies:     AdCurrencies;
  languages:      NlLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface NlLanguages {
  nld: string;
}

export interface NlNativeNames {
  nld: Cat;
}

export interface No {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    NoNativeNames;
  currencies:     BvCurrencies;
  languages:      NoLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface NoLanguages {
  nno: string;
  nob: string;
  smi: string;
}

export interface NoNativeNames {
  nno: Cat;
  nob: Cat;
  smi: Cat;
}

export interface Np {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           GwGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    NPNativeNames;
  currencies:     NPCurrencies;
  languages:      NPLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface NPCurrencies {
  NPR: Eur;
}

export interface NPLanguages {
  nep: string;
}

export interface NPNativeNames {
  nep: Cat;
}

export interface Nr {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           CDGini;
  timezones:      string[];
  borders:        string;
  nativeNames:    NrNativeNames;
  currencies:     AuCurrencies;
  languages:      NrLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface NrLanguages {
  eng: EngEnum;
  nau: string;
}

export interface NrNativeNames {
  eng: Cat;
  nau: Cat;
}

export interface Nu {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    NuNativeNames;
  currencies:     NuCurrencies;
  languages:      NuLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface NuCurrencies {
  NZD: Eur;
}

export interface NuLanguages {
  eng: EngEnum;
  niu: string;
}

export interface NuNativeNames {
  eng: Cat;
  niu: Cat;
}

export interface Nz {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    NzNativeNames;
  currencies:     NuCurrencies;
  languages:      NzLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface NzLanguages {
  eng: EngEnum;
  mri: string;
  nzs: string;
}

export interface NzNativeNames {
  eng: Cat;
  mri: Cat;
  nzs: Cat;
}

export interface Om {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AENativeNames;
  currencies:     OmCurrencies;
  languages:      AELanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface OmCurrencies {
  OMR: Eur;
}

export interface Pa {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     Alpha2Code;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AmGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    ClNativeNames;
  currencies:     { [key: string]: Eur };
  languages:      ClLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface Pe {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AmGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    BoNativeNames;
  currencies:     PECurrencies;
  languages:      BoLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface PECurrencies {
  PEN: Eur;
}

export interface PG {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           MlGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    PGNativeNames;
  currencies:     PGCurrencies;
  languages:      PGLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface PGCurrencies {
  PGK: Eur;
}

export interface PGLanguages {
  eng: EngEnum;
  hmo: string;
  tpi: string;
}

export interface PGNativeNames {
  eng: Cat;
  hmo: Cat;
  tpi: Cat;
}

export interface Ph {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string;
  nativeNames:    PhNativeNames;
  currencies:     PhCurrencies;
  languages:      PhLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface PhCurrencies {
  PHP: Eur;
}

export interface PhLanguages {
  eng: EngEnum;
  fil: string;
}

export interface PhNativeNames {
  eng: Cat;
  fil: Cat;
}

export interface Pk {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    PkNativeNames;
  currencies:     PkCurrencies;
  languages:      PkLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface PkCurrencies {
  PKR: Eur;
}

export interface PkLanguages {
  eng: EngEnum;
  urd: string;
}

export interface PkNativeNames {
  eng: Cat;
  urd: Cat;
}

export interface Pl {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    PlNativeNames;
  currencies:     PlCurrencies;
  languages:      PlLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface PlCurrencies {
  PLN: Eur;
}

export interface PlLanguages {
  pol: string;
}

export interface PlNativeNames {
  pol: Cat;
}

export interface Pn {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    AgNativeNames;
  currencies:     NuCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface Pr {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    PRNativeNames;
  currencies:     AsCurrencies;
  languages:      PRLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface PRLanguages {
  eng: EngEnum;
  spa: SPA;
}

export interface PRNativeNames {
  eng: Cat;
  spa: Cat;
}

export interface Ps {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BdGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AENativeNames;
  currencies:     PSCurrencies;
  languages:      AELanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface PSCurrencies {
  EGP: Eur;
  ILS: Eur;
  JOD: Eur;
}

export interface Pt {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AoNativeNames;
  currencies:     AdCurrencies;
  languages:      AoLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface Pw {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    PwNativeNames;
  currencies:     AsCurrencies;
  languages:      PwLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface PwLanguages {
  eng: EngEnum;
  pau: string;
}

export interface PwNativeNames {
  eng: Cat;
  pau: Cat;
}

export interface Py {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AmGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    ArNativeNames;
  currencies:     PyCurrencies;
  languages:      ArLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface PyCurrencies {
  PYG: Eur;
}

export interface Qa {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AENativeNames;
  currencies:     QACurrencies;
  languages:      AELanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface QACurrencies {
  QAR: Eur;
}

export interface Re {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    BfNativeNames;
  currencies:     AdCurrencies;
  languages:      BfLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface Ro {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    MdNativeNames;
  currencies:     RoCurrencies;
  languages:      MdLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface RoCurrencies {
  RON: Eur;
}

export interface Rs {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AlGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    RsNativeNames;
  currencies:     RsCurrencies;
  languages:      RsLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface RsCurrencies {
  RSD: Eur;
}

export interface RsLanguages {
  srp: string;
}

export interface RsNativeNames {
  srp: Cat;
}

export interface Ru {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    RuNativeNames;
  currencies:     RuCurrencies;
  languages:      RuLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface RuCurrencies {
  RUB: Eur;
}

export interface RuLanguages {
  rus: string;
}

export interface RuNativeNames {
  rus: Cat;
}

export interface Rw {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BdGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    RwNativeNames;
  currencies:     RwCurrencies;
  languages:      RwLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface RwCurrencies {
  RWF: Eur;
}

export interface RwLanguages {
  eng: EngEnum;
  fra: Fra;
  kin: string;
}

export interface RwNativeNames {
  eng: Cat;
  fra: Cat;
  kin: Cat;
}

export interface Sa {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AENativeNames;
  currencies:     SaCurrencies;
  languages:      AELanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface SaCurrencies {
  SAR: Eur;
}

export interface Sb {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           CDGini;
  timezones:      string[];
  borders:        string;
  nativeNames:    AgNativeNames;
  currencies:     SbCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface SbCurrencies {
  SBD: Eur;
}

export interface Sc {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string;
  nativeNames:    ScNativeNames;
  currencies:     ScCurrencies;
  languages:      ScLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface ScCurrencies {
  SCR: Eur;
}

export interface ScLanguages {
  crs: string;
  eng: EngEnum;
  fra: Fra;
}

export interface ScNativeNames {
  crs: Cat;
  eng: Cat;
  fra: Cat;
}

export interface Sd {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AuGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    SDNativeNames;
  currencies:     SDCurrencies;
  languages:      SDLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface SDCurrencies {
  SDG: BAM;
}

export interface SDLanguages {
  ara: Ara;
  eng: EngEnum;
}

export interface SDNativeNames {
  ara: Cat;
  eng: Cat;
}

export interface Se {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AxNativeNames;
  currencies:     SECurrencies;
  languages:      AxLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface SECurrencies {
  SEK: Eur;
}

export interface Sg {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    SgNativeNames;
  currencies:     SgCurrencies;
  languages:      SgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface SgCurrencies {
  SGD: Eur;
}

export interface SgLanguages {
  zho: string;
  eng: EngEnum;
  msa: string;
  tam: string;
}

export interface SgNativeNames {
  zho: Cat;
  eng: Cat;
  msa: Cat;
  tam: Cat;
}

export interface Sh {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    AgNativeNames;
  currencies:     ShCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface ShCurrencies {
  GBP: Eur;
  SHP: Eur;
}

export interface Si {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    SiNativeNames;
  currencies:     AdCurrencies;
  languages:      SiLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface SiLanguages {
  slv: string;
}

export interface SiNativeNames {
  slv: Cat;
}

export interface Sj {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       CcDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    BvNativeNames;
  currencies:     BvCurrencies;
  languages:      BvLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface Sk {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    SkNativeNames;
  currencies:     AdCurrencies;
  languages:      SkLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface SkLanguages {
  slk: string;
}

export interface SkNativeNames {
  slk: Cat;
}

export interface Sl {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AgNativeNames;
  currencies:     SlCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface SlCurrencies {
  SLL: Eur;
}

export interface Sm {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string[];
  nativeNames:    ItNativeNames;
  currencies:     AdCurrencies;
  languages:      ItLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface Sn {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BaGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    BfNativeNames;
  currencies:     BfCurrencies;
  languages:      BfLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface So {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AlGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    SoNativeNames;
  currencies:     SoCurrencies;
  languages:      SoLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface SoCurrencies {
  SOS: Eur;
}

export interface SoLanguages {
  ara: Ara;
  som: string;
}

export interface SoNativeNames {
  ara: Cat;
  som: Cat;
}

export interface Sr {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BzGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    NlNativeNames;
  currencies:     SrCurrencies;
  languages:      NlLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface SrCurrencies {
  SRD: Eur;
}

export interface Ss {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BdGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AgNativeNames;
  currencies:     SsCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface SsCurrencies {
  SSP: Eur;
}

export interface St {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AlGini;
  timezones:      string[];
  borders:        string;
  nativeNames:    AoNativeNames;
  currencies:     StCurrencies;
  languages:      AoLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface StCurrencies {
  STN: Eur;
}

export interface Sx {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string[];
  nativeNames:    SxNativeNames;
  currencies:     CwCurrencies;
  languages:      SxLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface SxLanguages {
  eng: EngEnum;
  fra: Fra;
  nld: string;
}

export interface SxNativeNames {
  eng: Cat;
  fra: Cat;
  nld: Cat;
}

export interface Sy {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           SyGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AENativeNames;
  currencies:     SyCurrencies;
  languages:      AELanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface SyCurrencies {
  SYP: Eur;
}

export interface SyGini {
  "2003": number;
}

export interface Sz {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BdGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    SzNativeNames;
  currencies:     SzCurrencies;
  languages:      SzLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface SzCurrencies {
  SZL: Eur;
  ZAR: Eur;
}

export interface SzLanguages {
  eng: EngEnum;
  ssw: string;
}

export interface SzNativeNames {
  eng: Cat;
  ssw: Cat;
}

export interface Tc {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       CcDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    AgNativeNames;
  currencies:     AsCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface Td {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BaGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    DjNativeNames;
  currencies:     CFCurrencies;
  languages:      DjLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface Th {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AmGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    ThNativeNames;
  currencies:     ThCurrencies;
  languages:      ThLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface ThCurrencies {
  THB: Eur;
}

export interface ThLanguages {
  tha: string;
}

export interface ThNativeNames {
  tha: Cat;
}

export interface Tj {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BjGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    TjNativeNames;
  currencies:     TjCurrencies;
  languages:      TjLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface TjCurrencies {
  TJS: Eur;
}

export interface TjLanguages {
  rus: string;
  tgk: string;
}

export interface TjNativeNames {
  rus: Cat;
  tgk: Cat;
}

export interface Tk {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       CcDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    TkNativeNames;
  currencies:     NuCurrencies;
  languages:      TkLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface TkLanguages {
  eng: EngEnum;
  smo: string;
  tkl: string;
}

export interface TkNativeNames {
  eng: Cat;
  smo: Cat;
  tkl: Cat;
}

export interface Tl {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AuGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    TlNativeNames;
  currencies:     AsCurrencies;
  languages:      TlLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface TlLanguages {
  por: string;
  tet: string;
}

export interface TlNativeNames {
  por: Cat;
  tet: Cat;
}

export interface Tm {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           GyGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    TmNativeNames;
  currencies:     TmCurrencies;
  languages:      TmLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface TmCurrencies {
  TMT: Eur;
}

export interface TmLanguages {
  rus: string;
  tuk: string;
}

export interface TmNativeNames {
  rus: Cat;
  tuk: Cat;
}

export interface Tn {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BjGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AENativeNames;
  currencies:     TnCurrencies;
  languages:      AELanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface TnCurrencies {
  TND: Eur;
}

export interface To {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BjGini;
  timezones:      string[];
  borders:        string;
  nativeNames:    ToNativeNames;
  currencies:     ToCurrencies;
  languages:      ToLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface ToCurrencies {
  TOP: Eur;
}

export interface ToLanguages {
  eng: EngEnum;
  ton: string;
}

export interface ToNativeNames {
  eng: Cat;
  ton: Cat;
}

export interface Tr {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AmGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    TrNativeNames;
  currencies:     TrCurrencies;
  languages:      TrLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface TrCurrencies {
  TRY: Eur;
}

export interface TrLanguages {
  tur: string;
}

export interface TrNativeNames {
  tur: Cat;
}

export interface Tt {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           TtGini;
  timezones:      string[];
  borders:        string;
  nativeNames:    AgNativeNames;
  currencies:     TtCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface TtCurrencies {
  TTD: Eur;
}

export interface TtGini {
  "1992": number;
}

export interface Tv {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           GwGini;
  timezones:      string[];
  borders:        string;
  nativeNames:    { [key: string]: Cat };
  currencies:     TvCurrencies;
  languages:      TvLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface TvCurrencies {
  AUD: Eur;
  TVD: Eur;
}

export interface TvLanguages {
  eng: EngEnum;
  tvl: string;
}

export interface Tw {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    CNNativeNames;
  currencies:     TwCurrencies;
  languages:      CNLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface TwCurrencies {
  TWD: Eur;
}

export interface Tz {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AlGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    KeNativeNames;
  currencies:     TzCurrencies;
  languages:      KeLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface TzCurrencies {
  TZS: Eur;
}

export interface Ua {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AmGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    UaNativeNames;
  currencies:     UaCurrencies;
  languages:      UaLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface UaCurrencies {
  UAH: Eur;
}

export interface UaLanguages {
  ukr: string;
}

export interface UaNativeNames {
  ukr: Cat;
}

export interface Ug {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BdGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    KeNativeNames;
  currencies:     UgCurrencies;
  languages:      KeLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface UgCurrencies {
  UGX: Eur;
}

export interface Um {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AqLatLng;
  demonyms:       CcDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    AgNativeNames;
  currencies:     AsCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface Us {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AgNativeNames;
  currencies:     AsCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface Uy {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AmGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    ClNativeNames;
  currencies:     UyCurrencies;
  languages:      ClLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface UyCurrencies {
  UYU: Eur;
}

export interface Uz {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           SyGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    UzNativeNames;
  currencies:     UzCurrencies;
  languages:      UzLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface UzCurrencies {
  UZS: Eur;
}

export interface UzLanguages {
  rus: string;
  uzb: string;
}

export interface UzNativeNames {
  rus: Cat;
  uzb: Cat;
}

export interface Va {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string[];
  nativeNames:    VaNativeNames;
  currencies:     AdCurrencies;
  languages:      VaLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface VaLanguages {
  ita: string;
  lat: string;
}

export interface VaNativeNames {
  ita: Cat;
  lat: Cat;
}

export interface Ve {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           KiGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    ClNativeNames;
  currencies:     VeCurrencies;
  languages:      ClLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface VeCurrencies {
  VES: Eur;
}

export interface Vn {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AEGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    VnNativeNames;
  currencies:     VnCurrencies;
  languages:      VnLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface VnCurrencies {
  VND: Eur;
}

export interface VnLanguages {
  vie: string;
}

export interface VnNativeNames {
  vie: Cat;
}

export interface Vu {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           GwGini;
  timezones:      string[];
  borders:        string;
  nativeNames:    VuNativeNames;
  currencies:     VuCurrencies;
  languages:      VuLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface VuCurrencies {
  VUV: Eur;
}

export interface VuLanguages {
  bis: string;
  eng: EngEnum;
  fra: Fra;
}

export interface VuNativeNames {
  bis: Cat;
  eng: Cat;
  fra: Cat;
}

export interface Wf {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       CcDemonyms;
  area:           number;
  gini:           string;
  timezones:      string[];
  borders:        string;
  nativeNames:    BfNativeNames;
  currencies:     NcCurrencies;
  languages:      BfLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface Ws {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BIGini;
  timezones:      string[];
  borders:        string;
  nativeNames:    AsNativeNames;
  currencies:     WsCurrencies;
  languages:      AsLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  string;
}

export interface WsCurrencies {
  WST: Eur;
}

export interface Xk {
  name:           string;
  official_name:  string;
  topLevelDomain: string;
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AlGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    { [key: string]: Cat };
  currencies:     AdCurrencies;
  languages:      XkLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AFRegionalBloc[];
}

export interface XkLanguages {
  sqi: string;
  srp: string;
}

export interface Ye {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AuGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AENativeNames;
  currencies:     YeCurrencies;
  languages:      AELanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface YeCurrencies {
  YER: Eur;
}

export interface Za {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AuGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    { [key: string]: Cat };
  currencies:     ZaCurrencies;
  languages:      ZaLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface ZaCurrencies {
  ZAR: Eur;
}

export interface ZaLanguages {
  afr: string;
  eng: EngEnum;
  nbl: string;
  nso: string;
  sot: string;
  ssw: string;
  tsn: string;
  tso: string;
  ven: string;
  xho: string;
  zul: string;
}

export interface Zm {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           BjGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    AgNativeNames;
  currencies:     ZmCurrencies;
  languages:      AgLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface ZmCurrencies {
  ZMW: Eur;
}

export interface Zw {
  name:           string;
  official_name:  string;
  topLevelDomain: string[];
  alpha2Code:     string;
  alpha3Code:     string;
  cioc:           string;
  numericCode:    string;
  callingCode:    string;
  capital:        string;
  altSpellings:   string[];
  region:         string;
  subregion:      string;
  population:     number;
  latLng:         AdLatLng;
  demonyms:       AdDemonyms;
  area:           number;
  gini:           AmGini;
  timezones:      string[];
  borders:        string[];
  nativeNames:    { [key: string]: Cat };
  currencies:     ZwCurrencies;
  languages:      ZwLanguages;
  translations:   Translations;
  flag:           Flag;
  regionalBlocs:  AERegionalBloc[];
}

export interface ZwCurrencies {
  ZWL: Eur;
}

export interface ZwLanguages {
  bwg: string;
  eng: EngEnum;
  kck: string;
  khi: string;
  ndc: string;
  nde: string;
  nya: string;
  sna: string;
  sot: string;
  toi: string;
  tsn: string;
  tso: string;
  ven: string;
  xho: string;
  zib: string;
}
