import { Country, Af, Translations } from "./pais.interface";

export function respuesta(res: Country, termino: string){
  let v = Object.keys(res);
  console.log(v);
  
  let resultado: any = [];

  for (let index = 0; index < v.length; index++) {
    switch (v[index]) {
      case "af": 
        resultado.push( pais(res,"af"));
        break;
      case "sd": 
        resultado.push( pais(res,"sd"));
        break;
      case "bi": 
        resultado.push( pais(res,"bi"));
        break;
      case "mx": 
        resultado.push( pais(res,"mx"));
        break;
      case "cu": 
        resultado.push( pais(res,"cu"));
        break;
      case "rs": 
        resultado.push( pais(res,"rs"));
        break;
      case "mc": 
        resultado.push( pais(res,"mc"));
        break;
      case "bt": 
        resultado.push( pais(res,"bt"));
        break;
      case "gy": 
        resultado.push( pais(res,"gy"));
        break;
      case "gs": 
        resultado.push( pais(res,"gs"));
        break;
      case "ba": 
        resultado.push( pais(res,"ba"));
        break;
      case "bn": 
        resultado.push( pais(res,"bn"));
        break;
      case "pk": 
        resultado.push( pais(res,"pk"));
        break;
      case "ke": 
        resultado.push( pais(res,"ke"));
        break;
      case "pr": 
        resultado.push( pais(res,"pr"));
        break;
      case "so": 
        resultado.push( pais(res,"so"));
        break;
      case "sj": 
        resultado.push( pais(res,"sj"));
        break;
      case "sl": 
        resultado.push( pais(res,"sl"));
        break;
      case "pf": 
        resultado.push( pais(res,"pf"));
        break;
      case "az": 
        resultado.push( pais(res,"az"));
        break;
      case "ck": 
        resultado.push( pais(res,"ck"));
        break;
      case "pe": 
        resultado.push( pais(res,"pe"));
        break;
      case "bv": 
        resultado.push( pais(res,"bv"));
        break;
      case "mp": 
        resultado.push( pais(res,"mp"));
        break;
      case "ao": 
        resultado.push( pais(res,"ao"));
        break;
      case "cg": 
        resultado.push( pais(res,"cg"));
        break;
      case "ss": 
        resultado.push( pais(res,"ss"));
        break;
      case "mf": 
        resultado.push( pais(res,"mf"));
        break;
      case "tr": 
        resultado.push( pais(res,"tr"));
        break;
      case "ai": 
        resultado.push( pais(res,"ai"));
        break;
      case "kn": 
        resultado.push( pais(res,"kn"));
        break;
      case "aw": 
        resultado.push( pais(res,"aw"));
        break;
      case "tc": 
        resultado.push( pais(res,"tc"));
        break;
      case "tw": 
        resultado.push( pais(res,"tw"));
        break;
      case "se": 
        resultado.push( pais(res,"se"));
        break;
      case "lr": 
        resultado.push( pais(res,"lr"));
        break;
      case "ve": 
        resultado.push( pais(res,"ve"));
        break;
      case "vi": 
        resultado.push( pais(res,"vi"));
        break;
      case "bm": 
        resultado.push( pais(res,"bm"));
        break;
      case "al": 
        resultado.push( pais(res,"al"));
        break;
      case "hk": 
        resultado.push( pais(res,"hk"));
        break;
      case "lu": 
        resultado.push( pais(res,"lu"));
        break;
      case "er": 
        resultado.push( pais(res,"er"));
        break;
      case "co": 
        resultado.push( pais(res,"co"));
        break;
      case "bq": 
        resultado.push( pais(res,"bq"));
        break;
      case "mn": 
        resultado.push( pais(res,"mn"));
        break;
      case "ye": 
        resultado.push( pais(res,"ye"));
        break;
      case "lb": 
        resultado.push( pais(res,"lb"));
        break;
      case "ag": 
        resultado.push( pais(res,"ag"));
        break;
      case "vn": 
        resultado.push( pais(res,"vn"));
        break;
      case "pw": 
        resultado.push( pais(res,"pw"));
        break;
      case "je": 
        resultado.push( pais(res,"je"));
        break;
      case "tt": 
        resultado.push( pais(res,"tt"));
        break;
      case "il": 
        resultado.push( pais(res,"il"));
        break;
      case "bg": 
        resultado.push( pais(res,"bg"));
        break;
      case "pt": 
        resultado.push( pais(res,"pt"));
        break;
      case "gi": 
        resultado.push( pais(res,"gi"));
        break;
      case "sm": 
        resultado.push( pais(res,"sm"));
        break;
      case "sg": 
        resultado.push( pais(res,"sg"));
        break;
      case "sx": 
        resultado.push( pais(res,"sx"));
        break;
      case "sa": 
        resultado.push( pais(res,"sa"));
        break;
      case "gh": 
        resultado.push( pais(res,"gh"));
        break;
      case "md": 
        resultado.push( pais(res,"md"));
        break;
      case "td": 
        resultado.push( pais(res,"td"));
        break;
      case "aq": 
        resultado.push( pais(res,"aq"));
        break;
      case "gb": 
        resultado.push( pais(res,"gb"));
        break;
      case "pg": 
        resultado.push( pais(res,"pg"));
        break;
      case "tf": 
        resultado.push( pais(res,"tf"));
        break;
      case "um": 
        resultado.push( pais(res,"um"));
        break;
      case "bz": 
        resultado.push( pais(res,"bz"));
        break;
      case "km": 
        resultado.push( pais(res,"km"));
        break;
      case "bf": 
        resultado.push( pais(res,"bf"));
        break;
      case "fo": 
        resultado.push( pais(res,"fo"));
        break;
      case "gn": 
        resultado.push( pais(res,"gn"));
        break;
      case "in": 
        resultado.push( pais(res,"in"));
        break;
      case "cw": 
        resultado.push( pais(res,"cw"));
        break;
      case "tg": 
        resultado.push( pais(res,"tg"));
        break;
      case "tn": 
        resultado.push( pais(res,"tn"));
        break;
      case "bl": 
        resultado.push( pais(res,"bl"));
        break;
      case "id": 
        resultado.push( pais(res,"id"));
        break;
      case "fm": 
        resultado.push( pais(res,"fm"));
        break;
      case "at": 
        resultado.push( pais(res,"at"));
        break;
      case "tj": 
        resultado.push( pais(res,"tj"));
        break;
      case "cd": 
        resultado.push( pais(res,"cd"));
        break;
      case "yt": 
        resultado.push( pais(res,"yt"));
        break;
      case "re": 
        resultado.push( pais(res,"re"));
        break;
      case "ro": 
        resultado.push( pais(res,"ro"));
        break;
      case "qa": 
        resultado.push( pais(res,"qa"));
        break;
      case "lt": 
        resultado.push( pais(res,"lt"));
        break;
      case "cn": 
        resultado.push( pais(res,"cn"));
        break;
      case "nz": 
        resultado.push( pais(res,"nz"));
        break;
      case "nf": 
        resultado.push( pais(res,"nf"));
        break;
      case "mr": 
        resultado.push( pais(res,"mr"));
        break;
      case "uz": 
        resultado.push( pais(res,"uz"));
        break;
      case "fi": 
        resultado.push( pais(res,"fi"));
        break;
      case "cm": 
        resultado.push( pais(res,"cm"));
        break;
      case "hm": 
        resultado.push( pais(res,"hm"));
        break;
      case "dm": 
        resultado.push( pais(res,"dm"));
        break;
      case "is": 
        resultado.push( pais(res,"is"));
        break;
      case "om": 
        resultado.push( pais(res,"om"));
        break;
      case "mk": 
        resultado.push( pais(res,"mk"));
        break;
      case "li": 
        resultado.push( pais(res,"li"));
        break;
      case "es": 
        resultado.push( pais(res,"es"));
        break;
      case "gr": 
        resultado.push( pais(res,"gr"));
        break;
      case "py": 
        resultado.push( pais(res,"py"));
        break;
      case "bh": 
        resultado.push( pais(res,"bh"));
        break;
      case "nu": 
        resultado.push( pais(res,"nu"));
        break;
      case "sn": 
        resultado.push( pais(res,"sn"));
        break;
      case "ms": 
        resultado.push( pais(res,"ms"));
        break;
      case "lv": 
        resultado.push( pais(res,"lv"));
        break;
      case "tk": 
        resultado.push( pais(res,"tk"));
        break;
      case "jp": 
        resultado.push( pais(res,"jp"));
        break;
      case "cf": 
        resultado.push( pais(res,"cf"));
        break;
      case "ga": 
        resultado.push( pais(res,"ga"));
        break;
      case "iq": 
        resultado.push( pais(res,"iq"));
        break;
      case "im": 
        resultado.push( pais(res,"im"));
        break;
      case "mm": 
        resultado.push( pais(res,"mm"));
        break;
      case "me": 
        resultado.push( pais(res,"me"));
        break;
      case "nr": 
        resultado.push( pais(res,"nr"));
        break;
      case "vu": 
        resultado.push( pais(res,"vu"));
        break;
      case "fr": 
        resultado.push( pais(res,"fr"));
        break;
      case "zw": 
        resultado.push( pais(res,"zw"));
        break;
      case "ph": 
        resultado.push( pais(res,"ph"));
        break;
      case "sk": 
        resultado.push( pais(res,"sk"));
        break;
      case "au": 
        resultado.push( pais(res,"au"));
        break;
      case "ci": 
        resultado.push( pais(res,"ci"));
        break;
      case "io": 
        resultado.push( pais(res,"io"));
        break;
      case "sz": 
        resultado.push( pais(res,"sz"));
        break;
      case "rw": 
        resultado.push( pais(res,"rw"));
        break;
      case "bj": 
        resultado.push( pais(res,"bj"));
        break;
      case "vg": 
        resultado.push( pais(res,"vg"));
        break;
      case "ug": 
        resultado.push( pais(res,"ug"));
        break;
      case "ie": 
        resultado.push( pais(res,"ie"));
        break;
      case "ir": 
        resultado.push( pais(res,"ir"));
        break;
      case "si": 
        resultado.push( pais(res,"si"));
        break;
      case "ml": 
        resultado.push( pais(res,"ml"));
        break;
      case "ch": 
        resultado.push( pais(res,"ch"));
        break;
      case "as": 
        resultado.push( pais(res,"as"));
        break;
      case "uy": 
        resultado.push( pais(res,"uy"));
        break;
      case "gu": 
        resultado.push( pais(res,"gu"));
        break;
      case "sr": 
        resultado.push( pais(res,"sr"));
        break;
      case "ua": 
        resultado.push( pais(res,"ua"));
        break;
      case "cz": 
        resultado.push( pais(res,"cz"));
        break;
      case "hn": 
        resultado.push( pais(res,"hn"));
        break;
      case "ws": 
        resultado.push( pais(res,"ws"));
        break;
      case "la": 
        resultado.push( pais(res,"la"));
        break;
      case "cv": 
        resultado.push( pais(res,"cv"));
        break;
      case "et": 
        resultado.push( pais(res,"et"));
        break;
      case "bd": 
        resultado.push( pais(res,"bd"));
        break;
      case "sh": 
        resultado.push( pais(res,"sh"));
        break;
      case "by": 
        resultado.push( pais(res,"by"));
        break;
      case "hr": 
        resultado.push( pais(res,"hr"));
        break;
      case "kw": 
        resultado.push( pais(res,"kw"));
        break;
      case "gf": 
        resultado.push( pais(res,"gf"));
        break;
      case "ma": 
        resultado.push( pais(res,"ma"));
        break;
      case "ru": 
        resultado.push( pais(res,"ru"));
        break;
      case "ee": 
        resultado.push( pais(res,"ee"));
        break;
      case "lk": 
        resultado.push( pais(res,"lk"));
        break;
      case "nc": 
        resultado.push( pais(res,"nc"));
        break;
      case "pl": 
        resultado.push( pais(res,"pl"));
        break;
      case "mg": 
        resultado.push( pais(res,"mg"));
        break;
      case "cr": 
        resultado.push( pais(res,"cr"));
        break;
      case "sv": 
        resultado.push( pais(res,"sv"));
        break;
      case "mo": 
        resultado.push( pais(res,"mo"));
        break;
      case "ad": 
        resultado.push( pais(res,"ad"));
        break;
      case "it": 
        resultado.push( pais(res,"it"));
        break;
      case "na": 
        resultado.push( pais(res,"na"));
        break;
      case "sc": 
        resultado.push( pais(res,"sc"));
        break;
      case "pa": 
        resultado.push( pais(res,"pa"));
        break;
      case "ht": 
        resultado.push( pais(res,"ht"));
        break;
      case "ar": 
        resultado.push( pais(res,"ar"));
        break;
      case "ne": 
        resultado.push( pais(res,"ne"));
        break;
      case "mw": 
        resultado.push( pais(res,"mw"));
        break;
      case "pn": 
        resultado.push( pais(res,"pn"));
        break;
      case "de": 
        resultado.push( pais(res,"de"));
        break;
      case "ki": 
        resultado.push( pais(res,"ki"));
        break;
      case "sy": 
        resultado.push( pais(res,"sy"));
        break;
      case "mh": 
        resultado.push( pais(res,"mh"));
        break;
      case "hu": 
        resultado.push( pais(res,"hu"));
        break;
      case "ky": 
        resultado.push( pais(res,"ky"));
        break;
      case "dk": 
        resultado.push( pais(res,"dk"));
        break;
      case "lc": 
        resultado.push( pais(res,"lc"));
        break;
      case "bo": 
        resultado.push( pais(res,"bo"));
        break;
      case "dj": 
        resultado.push( pais(res,"dj"));
        break;
      case "za": 
        resultado.push( pais(res,"za"));
        break;
      case "ng": 
        resultado.push( pais(res,"ng"));
        break;
      case "st": 
        resultado.push( pais(res,"st"));
        break;
      case "ni": 
        resultado.push( pais(res,"ni"));
        break;
      case "gp": 
        resultado.push( pais(res,"gp"));
        break;
      case "pm": 
        resultado.push( pais(res,"pm"));
        break;
      case "ec": 
        resultado.push( pais(res,"ec"));
        break;
      case "gl": 
        resultado.push( pais(res,"gl"));
        break;
      case "gd": 
        resultado.push( pais(res,"gd"));
        break;
      case "bs": 
        resultado.push( pais(res,"bs"));
        break;
      case "cl": 
        resultado.push( pais(res,"cl"));
        break;
      case "my": 
        resultado.push( pais(res,"my"));
        break;
      case "tv": 
        resultado.push( pais(res,"tv"));
        break;
      case "cx": 
        resultado.push( pais(res,"cx"));
        break;
      case "sb": 
        resultado.push( pais(res,"sb"));
        break;
      case "tz": 
        resultado.push( pais(res,"tz"));
        break;
      case "kp": 
        resultado.push( pais(res,"kp"));
        break;
      case "gw": 
        resultado.push( pais(res,"gw"));
        break;
      case "xk": 
        resultado.push( pais(res,"xk"));
        break;
      case "va": 
        resultado.push( pais(res,"va"));
        break;
      case "no": 
        resultado.push( pais(res,"no"));
        break;
      case "ps": 
        resultado.push( pais(res,"ps"));
        break;
      case "cc": 
        resultado.push( pais(res,"cc"));
        break;
      case "jm": 
        resultado.push( pais(res,"jm"));
        break;
      case "eg": 
        resultado.push( pais(res,"eg"));
        break;
      case "kh": 
        resultado.push( pais(res,"kh"));
        break;
      case "mu": 
        resultado.push( pais(res,"mu"));
        break;
      case "gm": 
        resultado.push( pais(res,"gm"));
        break;
      case "gq": 
        resultado.push( pais(res,"gq"));
        break;
      case "ls": 
        resultado.push( pais(res,"ls"));
        break;
      case "mq": 
        resultado.push( pais(res,"mq"));
        break;
      case "us": 
        resultado.push( pais(res,"us"));
        break;
      case "eh": 
        resultado.push( pais(res,"eh"));
        break;
      case "ae": 
        resultado.push( pais(res,"ae"));
        break;
      case "mz": 
        resultado.push( pais(res,"mz"));
        break;
      case "dz": 
        resultado.push( pais(res,"dz"));
        break;
      case "zm": 
        resultado.push( pais(res,"zm"));
        break;
      case "gt": 
        resultado.push( pais(res,"gt"));
        break;
      case "kr": 
        resultado.push( pais(res,"kr"));
        break;
      case "kg": 
        resultado.push( pais(res,"kg"));
        break;
      case "tl": 
        resultado.push( pais(res,"tl"));
        break;
      case "ax": 
        resultado.push( pais(res,"ax"));
        break;
      case "jo": 
        resultado.push( pais(res,"jo"));
        break;
      case "mt": 
        resultado.push( pais(res,"mt"));
        break;
      case "cy": 
        resultado.push( pais(res,"cy"));
        break;
      case "fk": 
        resultado.push( pais(res,"fk"));
        break;
      case "kz": 
        resultado.push( pais(res,"kz"));
        break;
      case "bw": 
        resultado.push( pais(res,"bw"));
        break;
      case "vc": 
        resultado.push( pais(res,"vc"));
        break;
      case "bb": 
        resultado.push( pais(res,"bb"));
        break;
      case "to": 
        resultado.push( pais(res,"to"));
        break;
      case "th": 
        resultado.push( pais(res,"th"));
        break;
      case "be": 
        resultado.push( pais(res,"be"));
        break;
      case "ca": 
        resultado.push( pais(res,"ca"));
        break;
      case "ge": 
        resultado.push( pais(res,"ge"));
        break;
      case "wf": 
        resultado.push( pais(res,"wf"));
        break;
      case "fj": 
        resultado.push( pais(res,"fj"));
        break;
      case "nl": 
        resultado.push( pais(res,"nl"));
        break;
      case "am": 
        resultado.push( pais(res,"am"));
        break;
      case "do": 
        resultado.push( pais(res,"do"));
        break;
      case "gg": 
        resultado.push( pais(res,"gg"));
        break;
      case "tm": 
        resultado.push( pais(res,"tm"));
        break;
      case "np": 
        resultado.push( pais(res,"np"));
        break;
      case "mv": 
        resultado.push( pais(res,"mv"));
        break;
      case "ly": 
        resultado.push( pais(res,"ly"));
        break;
  
      case "br": 
        resultado.push( pais(res,"br"));
        break;
    }
  }

  return resultado;
}

function pais(res: Country, termino:keyof Country) {
  return res[termino];
}

export function returnLengua(response: any){
  
  return response.spa;
}
  